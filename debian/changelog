libosmocore (0.12.1-3) unstable; urgency=low

  [Ruben Undheim]
  * Attempt fix FTBFS on kFreeBSD and Hurd:
    - d/patches/0007-TIMER-constants-not-on-some-architectures.patch
    - d/patches/MAXPATHLEN-set-if-not-defined.patch
    - d/patches/0009-No-fail-if-no-proc-cpuinfo.patch

  [Thorsten Alteholz]
  * add patch to build with gcc9 (cherry picked from upstream)
    (Closes: #925756)
  * add "* Build-Depends-Package:" to symbols files

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 18 Aug 2019 10:55:55 +0200

libosmocore (0.12.1-2) unstable; urgency=medium

  * debian/rules:
    - Enable test suite on big-endian architectures
  * Two new patches to fix test suite on big-endian architectures:
    - 0005-Structs-for-big-endian.patch
    - 0006-Fix-some-byte-ordering-for-big-endian-architectures.patch

 -- Ruben Undheim <ruben.undheim@gmail.com>  Wed, 14 Nov 2018 23:38:45 +0100

libosmocore (0.12.1-1) unstable; urgency=medium

  * Upload to unstable

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 05 Nov 2018 21:21:01 +0100

libosmocore (0.12.1-1~exp2) experimental; urgency=medium

  * debian/control:
    - New standards version 4.2.1 - no changes
    - Update Breaks+Replaces relation for libosmocoding to
      libosmocore9 (<< 0.10.2-7). This will allow libosmocore9 (sid) and
      libosmocoding0 (experimental) to be installed at the same time.
  * debian/changelog:
    - New changelog entry from unstable (0.10.2-7) inserted in the changelog
      (sorted by version)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 30 Sep 2018 12:35:59 +0200

libosmocore (0.12.1-1~exp1) experimental; urgency=medium

  * New upstream release
  * Drop patch debian/patches/0005-Fix-erronous-LIBVERSION-for-libosmovty.patch
    since fixed upstream

 -- Ruben Undheim <ruben.undheim@gmail.com>  Wed, 29 Aug 2018 19:32:29 +0200

libosmocore (0.12.0-1~exp1) experimental; urgency=medium

  * New upstream release
  * Refreshed patches
  * debian/changelog_upstream added
  * debian/control:
    - New standards version 4.2.0 - no changes
    - New binary package libosmoctrl-doc
    - libosmocore10 -> libosmocore11
    - libosmogsm9 -> libosmogsm10
  * debian/copyright:
    - Added copyright information for two new files
    - Updated copyright years for maintainers
  * debian/libosmoctrl-doc.doc-base added
  * debian/patches/0005-Fix-erronous-LIBVERSION-for-libosmovty.patch:
    - Fixing erroneous LIBVERSION set by upstream
  * debian/rules:
    - Delete more in override_dh_clean such that the build directory
      is properly cleaned up.
    - Override override_dh_installchangelogs such that the upstream
      changelog gets installed.
    - Use /usr/share/dpkg/pkg-info.mk instead of dpkg-parsechangelog

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sat, 25 Aug 2018 14:06:35 +0200

libosmocore (0.11.0-2~exp2) experimental; urgency=medium

  * Add libosmocoding0 as a dependency for libosmocore-dev (Closes: #905043)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 02 Aug 2018 21:08:05 +0200

libosmocore (0.11.0-2~exp1) experimental; urgency=medium

  * debian/control:
    - Add Breaks+Replaces relation for libosmocoding0:
        libosmocore9 (<< 0.11.0-1) (Closes: #901216)
    - New standards version 4.1.5 - no changes

 -- Ruben Undheim <ruben.undheim@gmail.com>  Thu, 12 Jul 2018 08:54:27 +0200

libosmocore (0.11.0-1) experimental; urgency=medium

  * New upstream release
  * upload to experimental
  * debian/control: add new dependency of gnutls
  * debian/patches: add new spelling patch
  * soname bumps:
    * libosmogsm8 -> libosmogms9
    * libosmogb5   -> libosmogb6
    * libosmocore9 -> libosmocore10
  * new library: libosmocoding0 (Closes: #897278)
        debian/* files taken from upstream

 -- Thorsten Alteholz <debian@alteholz.de>  Mon, 14 May 2018 19:55:55 +0200

libosmocore (0.10.2-8) unstable; urgency=medium

  * debian/control:
    - Dependency on libosmocoding0 should not be tied to specific version.

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 30 Sep 2018 14:13:55 +0200

libosmocore (0.10.2-7) unstable; urgency=medium

  * Put libosmocoding0 into separate package such as in experimental
    - This should ease migration. libosmocore9 (sid) and libosmocoding0
      (experimental) can be installed at the same time.
  * Some trivial lintian fixes (whitespaces, Vcs URL etc.)
  * debian/rules:
    - Properly clean up in make target

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 30 Sep 2018 12:20:39 +0200

libosmocore (0.10.2-6) unstable; urgency=medium

  * add dependency of libtalloc-dev to libsomocore-dev (Closes: #895178)

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 08 Apr 2018 12:05:14 +0100

libosmocore (0.10.2-5) unstable; urgency=medium

  * add path-max.patch
  * debian/rules: fix typo for s390x
  * debian/rules: add sparc64 to no-test
  * endian bug was already fixed in 0.10.2-4 (Closes: #895118)

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 07 Apr 2018 19:07:14 +0100

libosmocore (0.10.2-4) unstable; urgency=medium

  * debian/rules: deactivate testsuite for big endian architecture
                  s390x and mips from supported archs
                  lib has problem with one endian related test (gsm0808)
  * add libosmocoding to libosmocore9 (Closes: #891345)
  * add big-endian-problem.patch

 -- Thorsten Alteholz <debian@alteholz.de>  Sat, 07 Apr 2018 11:07:14 +0100

libosmocore (0.10.2-3) unstable; urgency=medium

  * move to unstable
  * debian/control: add salsa URLs
  * debian/control: use dh11
  * debian/control: bump standard to 4.1.4 (no changes)

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 05 Apr 2018 19:17:14 +0100

libosmocore (0.10.2-2) experimental; urgency=medium

  * adapt 0003-Setting-library-version-explicitly.patch to
    this version

 -- Thorsten Alteholz <debian@alteholz.de>  Sun, 03 Dec 2017 16:54:26 +0100

libosmocore (0.10.2-1) experimental; urgency=medium

  * New upstream release
  * debian/control: change dependency to python (for argparse)
  * debian/control: use dh10
  * debian/control: bump standard to 4.1.1
  * debian/control: remove dependencies on autotools-dev, dh-autoreconf
  * debian/control: remove useless dbg package
  * debian/*.symbols: update symbols files
  * debian/copyright: update

 -- Thorsten Alteholz <debian@alteholz.de>  Thu, 16 Nov 2017 18:54:26 +0100

libosmocore (0.9.6-1) experimental; urgency=medium

  * new upstream version
  * debian/control: add myself to uploaders
  * debian/control: move package to debian-mobcom and change URLs
  * debian/control: change maintainer to debian-mobcom-maintainers
  * refresh symbol files
  * use system wide libtalloc instead of internal copy
  * add python-minimal dependency

 -- Thorsten Alteholz <debian@alteholz.de>  Wed, 08 Mar 2017 19:52:59 +0100

libosmocore (0.9.0-6) unstable; urgency=medium

  * Disable unreliable tests (Closes: #842677)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Wed, 02 Nov 2016 22:52:59 +0100

libosmocore (0.9.0-5) unstable; urgency=medium

  * debian/patches/0004-Patched-struct-for-big-endian-architectures.patch:
    - Fixed even more structs for big-endian architectures which will
      cause #818566 to be fixed with a binNMU on openbsc.
  * debian/control:
    - New standards version: 3.9.8 - no changes

 -- Ruben Undheim <ruben.undheim@gmail.com>  Tue, 10 May 2016 22:42:43 +0200

libosmocore (0.9.0-4) unstable; urgency=medium

  * debian/control:
    - Changed Vcs-Git to use https
    - Updated standards to 3.9.7
    - Run "cme fix dpkg-control"
  * debian/rules:
    - Explicitly remove "-Wl,-Bsymbolic-functions" from LDFLAGS to prevent
      build problems on some Debian derivatives.
  * Added:
      d/patches/0005-Set-HTML_TIMESTAMP-to-NO-to-make-build-reproducible.patch

 -- Ruben Undheim <ruben.undheim@gmail.com>  Wed, 10 Feb 2016 20:24:50 +0100

libosmocore (0.9.0-3) unstable; urgency=medium

  * Uploaded to unstable
  * Fixes big-endian bug (Closes: #807137)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Wed, 09 Dec 2015 08:38:40 +0100

libosmocore (0.9.0-3~exp3) experimental; urgency=medium

  * debian/patches/0004-Patched-struct-for-big-endian-architectures.patch:
    - Third attempt at fixing the big-endian problem

 -- Ruben Undheim <ruben.undheim@gmail.com>  Tue, 08 Dec 2015 21:14:57 +0100

libosmocore (0.9.0-3~exp2) experimental; urgency=medium

  * debian/patches/0004-Patched-struct-for-big-endian-architectures.patch:
    - Another attempt at fixing the big-endian problem

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 07 Dec 2015 22:10:03 +0100

libosmocore (0.9.0-3~exp1) experimental; urgency=medium

  * Fix for struct on big-endian architectures:
    - debian/patches/0004-Patched-struct-for-big-endian-architectures.patch

 -- Ruben Undheim <ruben.undheim@gmail.com>  Mon, 07 Dec 2015 19:53:53 +0100

libosmocore (0.9.0-2) unstable; urgency=medium

  * debian/control
    - Fixed dependency of libosmovty-doc
  * Added patch debian/patches/0003-Setting-library-version-explicitly.patch
    - Makes sure the correct version is listed in the .pc files.

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 06 Dec 2015 14:54:37 +0100

libosmocore (0.9.0-1) unstable; urgency=medium

  * New upstream release
    - New SONAME of libosmovty (3)
  * debian/rules:
    - Fixed reproducibility (Closes: #806551)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Sun, 29 Nov 2015 13:53:16 +0100

libosmocore (0.8.3-1) unstable; urgency=low

  * Initial release (Closes: 646276)

 -- Ruben Undheim <ruben.undheim@gmail.com>  Wed, 14 Oct 2015 14:27:21 +0200
